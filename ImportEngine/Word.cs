﻿using System.Collections.Generic;

namespace IndexEngine
{
    public class Word
    {
        //
        // Data members
        //

        public string Text { get; set; }

        public Dictionary<string, WordInstance> wordInstanceDictionary { get; set; }

        //
        // Constructors
        //

        public Word(string text)
        {
            Text = text;
            wordInstanceDictionary = new Dictionary<string, WordInstance>();
        }
    }
}
