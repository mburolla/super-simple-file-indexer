﻿namespace IndexEngine
{
    public class BuildIndexResult
    {
        //
        // Data members
        //

        public int NumFilesIndexed { get; set; }

        public double ExecutionTimeSec { get; set; }

        public double CombinedFileSizeMB { get; set; }
    }
}
