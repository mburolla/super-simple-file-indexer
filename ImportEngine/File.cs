﻿using System.IO;
using System.Collections.Generic;

namespace IndexEngine
{
    public class File
    {
        //
        // Data
        //

        private string FileName;

        public string FullFilePath { get; set; }

        public Dictionary<string, Word> WordIndex { get; set; }

        //
        // Constructors
        //

        public File(string fullFilePath)
        {
            FullFilePath = fullFilePath;
            FileName = Path.GetFileName(FullFilePath);
        }

        //
        // Methods
        //

        public void BuildIndices()
        {
            WordIndex = new Dictionary<string, Word>();
            string[] words = BuildWordArrayFromFile();

            foreach (string word in words)
            {
                var w = new Word(word);

                if (!WordIndex.ContainsKey(w.Text))
                {
                    WordIndex.Add(w.Text, w);
                }

                if (!WordIndex[w.Text].wordInstanceDictionary.ContainsKey(FileName))
                {
                    var wordInstance = new WordInstance(0, FileName);
                    WordIndex[w.Text].wordInstanceDictionary.Add(FileName, wordInstance);
                }

                WordIndex[w.Text].wordInstanceDictionary[FileName].Count++;
            }
        }

        //
        // Helpers
        //

        private string[] BuildWordArrayFromFile()
        {
            string[] retval;

            using (StreamReader sr = new StreamReader(FullFilePath))
            {
                var fileContents = sr.ReadToEnd().ToLower();
                retval = fileContents.Split(' ');
            }

            return retval;
        }
    }
}
