﻿using System;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace IndexEngine
{
    public class IndexEngine
    {
        //
        // Data Members
        //

        private List<File> FileList;
        private bool HasIndexes { get; set; }

        //
        // Constructors
        //

        public IndexEngine(string theFileDirectory)
        {
            HasIndexes = false;
            FileList = new List<File>();

            foreach (string file in Directory.GetFiles(theFileDirectory))
            {
                var theFile = new File(file);
                FileList.Add(theFile);
            }
        }

        //
        // Methods
        //

        public async Task<BuildIndexResult> BuildIndices()
        {
            HasIndexes = false;
            long combinedFileSizeBytes = 0;
            List<Task> taskList = new List<Task>();
            BuildIndexResult retval = new BuildIndexResult();

            var stopWatch = Stopwatch.StartNew();
            foreach (File file in FileList)
            {
                taskList.Add(Task.Run(() => file.BuildIndices()));
                combinedFileSizeBytes += new FileInfo(file.FullFilePath).Length;
            }
            await Task.WhenAll(taskList); // Wait for all files to be indexed.
            stopWatch.Stop();

            HasIndexes = true;
            retval.NumFilesIndexed = FileList.Count;
            retval.ExecutionTimeSec = stopWatch.ElapsedMilliseconds / 1000.0;
            retval.CombinedFileSizeMB = Math.Round(combinedFileSizeBytes / (1024.0 * 1024.0), 2);
            return retval;
        }

        public List<SearchResult> SearchForWord(string text)
        {
            text = text.ToLower();
            List<SearchResult> retval = new List<SearchResult>();

            if (!HasIndexes)
            {
                throw new Exception("Indexes not loaded.");
            }
          
            foreach (File file in FileList)
            {
                if (file.WordIndex.ContainsKey(text))
                {
                    var word = file.WordIndex[text];
                    foreach (var key in word.wordInstanceDictionary.Keys)
                    {
                        retval.Add(new SearchResult(word.wordInstanceDictionary[key].File, word.wordInstanceDictionary[key].Count));
                    }
                }
            }
           
            return retval;
        }
    }
}
