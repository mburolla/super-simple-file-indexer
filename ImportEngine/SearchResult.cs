﻿namespace IndexEngine
{
    public class SearchResult
    {
        //
        // Data members
        //

        public string FileName { get; set; }

        public int WordCount { get; set; }

        //
        // Constructors
        //

        public SearchResult(string fileName, int wordCount)
        {
            FileName = fileName;
            WordCount = wordCount;
        }
    }
}
