﻿namespace IndexEngine
{
    public class WordInstance
    {
        //
        // Data members
        //

        public int Count { get; set; }

        public string File { get; set; }

        // 
        // Constructors
        //

        public WordInstance(int count, string file)
        {
            Count = count;
            File = file;
        }
    }
}
