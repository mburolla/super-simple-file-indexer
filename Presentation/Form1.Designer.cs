﻿namespace Presentation
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblSize = new System.Windows.Forms.Label();
            this.lblNumFiles = new System.Windows.Forms.Label();
            this.btnBuildIndices = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lsvResults = new System.Windows.Forms.ListView();
            this.FileName = new System.Windows.Forms.ColumnHeader();
            this.Count = new System.Windows.Forms.ColumnHeader();
            this.lblResults = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Enabled = false;
            this.btnSearch.Location = new System.Drawing.Point(294, 62);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(150, 46);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(22, 62);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(228, 42);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.Text = "The";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(6, 882);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblTime);
            this.groupBox1.Controls.Add(this.lblSize);
            this.groupBox1.Controls.Add(this.lblNumFiles);
            this.groupBox1.Controls.Add(this.btnBuildIndices);
            this.groupBox1.Location = new System.Drawing.Point(29, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(468, 194);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Indices";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(234, 144);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(145, 36);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "Time: 0 Sec";
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.Location = new System.Drawing.Point(234, 91);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(133, 36);
            this.lblSize.TabIndex = 2;
            this.lblSize.Text = "Size: 0 MB";
            // 
            // lblNumFiles
            // 
            this.lblNumFiles.AutoSize = true;
            this.lblNumFiles.Location = new System.Drawing.Point(234, 38);
            this.lblNumFiles.Name = "lblNumFiles";
            this.lblNumFiles.Size = new System.Drawing.Size(155, 36);
            this.lblNumFiles.TabIndex = 1;
            this.lblNumFiles.Text = "Num Files: 0";
            // 
            // btnBuildIndices
            // 
            this.btnBuildIndices.Location = new System.Drawing.Point(22, 50);
            this.btnBuildIndices.Name = "btnBuildIndices";
            this.btnBuildIndices.Size = new System.Drawing.Size(178, 46);
            this.btnBuildIndices.TabIndex = 0;
            this.btnBuildIndices.Text = "Build";
            this.btnBuildIndices.UseVisualStyleBackColor = true;
            this.btnBuildIndices.Click += new System.EventHandler(this.btnBuildIndices_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lsvResults);
            this.groupBox2.Controls.Add(this.lblResults);
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Controls.Add(this.txtSearch);
            this.groupBox2.Location = new System.Drawing.Point(29, 212);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(468, 658);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search";
            // 
            // lsvResults
            // 
            this.lsvResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.FileName,
            this.Count});
            this.lsvResults.GridLines = true;
            this.lsvResults.HideSelection = false;
            this.lsvResults.Location = new System.Drawing.Point(21, 163);
            this.lsvResults.Name = "lsvResults";
            this.lsvResults.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lsvResults.Size = new System.Drawing.Size(423, 470);
            this.lsvResults.TabIndex = 6;
            this.lsvResults.UseCompatibleStateImageBehavior = false;
            this.lsvResults.View = System.Windows.Forms.View.Details;
            // 
            // FileName
            // 
            this.FileName.Name = "FileName";
            this.FileName.Text = "Filename";
            this.FileName.Width = 280;
            // 
            // Count
            // 
            this.Count.Name = "Count";
            this.Count.Text = "Count";
            this.Count.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Count.Width = 100;
            // 
            // lblResults
            // 
            this.lblResults.AutoSize = true;
            this.lblResults.Location = new System.Drawing.Point(21, 124);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(96, 36);
            this.lblResults.TabIndex = 3;
            this.lblResults.Text = "Results";
            // 
            // Form1
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 36F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 882);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.splitter1);
            this.Name = "Form1";
            this.Text = "SS File Indexer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuildIndices;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblNumFiles;
        private System.Windows.Forms.Label lblResults;
        private System.Windows.Forms.ListView lsvResults;
        private System.Windows.Forms.ColumnHeader FileName;
        private System.Windows.Forms.ColumnHeader Count;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblSize;
    }
}

