﻿using System;
using System.Windows.Forms;
using IndexEngine;

namespace Presentation
{
    public partial class Form1 : Form
    {
        //
        // Data members
        //

        private IndexEngine.IndexEngine IndexEngine;

        // 
        // Constructors
        //

        public Form1()
        {
            IndexEngine = new IndexEngine.IndexEngine("..\\..\\..\\files\\");

            InitializeComponent();
        }

        //
        // Methods
        //

        /// <summary>
        /// Build Indices
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnBuildIndices_Click(object sender, EventArgs e)
        {
            var buildIndexResult = await IndexEngine.BuildIndices();
            lblNumFiles.Text = $"Num Files: {buildIndexResult.NumFilesIndexed}";
            lblSize.Text = $"Size: {buildIndexResult.CombinedFileSizeMB} MB";
            lblTime.Text = $"Time: {buildIndexResult.ExecutionTimeSec} Sec";
            btnSearch.Enabled = true;
        }

        /// <summary>
        /// Search for word
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            var searchResults = IndexEngine.SearchForWord(txtSearch.Text);

            lsvResults.Items.Clear();

            foreach (SearchResult sr in searchResults)
            {
                lsvResults.Items.Add(new ListViewItem(new string[] { sr.FileName, sr.WordCount.ToString() }));
            }
        }
    }
}
