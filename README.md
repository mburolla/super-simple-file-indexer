# Super Simple File Indexer
A multi-threaded index engine that catalogs the frequency of words in multiple text files.

<img src="https://gitlab.com/mburolla/super-simple-file-indexer/uploads/bc1bfca86b05fa9bca6721101a3e4ddf/SS_File_Indexer.png" width=200>

# Features
- .Net Core 3.0
- Async/Await and the Task Parallel Library (TPL)
- Dictionaries
- Speed!

# Misc
- Built with MS Visual Studio Version 16.6.0 Preview 2.1
