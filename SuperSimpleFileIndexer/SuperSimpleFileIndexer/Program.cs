﻿using IndexEngine;
using System;
using System.Threading.Tasks;

namespace SuperSimpleFileIndexer
{
    /// <summary>
    /// A test client that interacts with the Index Engine.
    /// </summary>
    class Program
    {
        static async Task Main()
        {
            // Build
            var indexEngine = new IndexEngine.IndexEngine("..\\..\\..\\files\\");
            await indexEngine.BuildIndices();
            
            // Search
            var searchText = "The";
            var searchReults = indexEngine.SearchForWord(searchText);
            var results = "";

            foreach(SearchResult sr in searchReults)
            {
                results += sr.FileName + " " + sr.WordCount + "\r\n";
            }

            // Report
            Console.WriteLine(results);
        }
    }
}
